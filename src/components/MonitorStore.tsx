import { MonitorFilters } from './MonitorFilters'
import { MonitorInventory } from './MonitorInventory'
import { NewMonitorForm } from './NewMonitorForm'
import { useState } from 'react'

export const BRAND_NAMES = ['Dell', 'HP', 'IBM', 'Lenovo'] as const
export const SCREEN_SIZES = [17, 19, 21, 23, 25] as const

export type BrandNameType = typeof BRAND_NAMES[number]
export type ScreenSizeType = typeof SCREEN_SIZES[number]

export type MonitorType = {
  productNumber: string
  brand: BrandNameType
  screenSize: ScreenSizeType
  price: number
  quantity: number
}

type MonitorStoreProps = {
  storeName: string
}

export type MonitorFiltersType = {
  brand: { active: boolean; value: BrandNameType | '' }
  minimumScreenSize: { active: boolean; value: ScreenSizeType | '' }
  maximumPrice: { active: boolean; value: number | '' }
}

const DEFAULT_MONITOR_FILTERS: MonitorFiltersType = {
  brand: { active: false, value: '' },
  minimumScreenSize: { active: false, value: '' },
  maximumPrice: { active: false, value: '' },
}

export type AddMonitorFunctionType = (monitorData: MonitorType) => Promise<void>

export type ToggleFilterFunctionType = (
  fieldName: keyof MonitorFiltersType
) => (event: React.ChangeEvent<HTMLInputElement>) => void

export type ChangeFilterValueFunctionType = (
  fieldName: keyof MonitorFiltersType
) => (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void

export type SellMonitorFunctionType = (productNumber: string) => Promise<number>

export const MonitorStore: React.FC<MonitorStoreProps> = ({ storeName }) => {
  const [monitors, setMonitors] = useState<MonitorType[]>([])
  const [monitorFilters, setMonitorFilters] = useState(DEFAULT_MONITOR_FILTERS)

  const handleAddMonitor: AddMonitorFunctionType = monitorData => {
    const monitorAlreadyExists =
      monitors.find(
        monitor => monitor.productNumber === monitorData.productNumber
      ) !== undefined

    if (monitorAlreadyExists)
      return Promise.reject(
        `Sorry, the product number "${monitorData.productNumber}" is already in use.`
      )

    setMonitors(previousMonitors => [...previousMonitors, monitorData])

    return Promise.resolve()
  }

  const handleToggleFilter: ToggleFilterFunctionType = fieldName => event => {
    setMonitorFilters(previousMonitorFilters => ({
      ...previousMonitorFilters,
      [fieldName]: {
        ...previousMonitorFilters[fieldName],
        active: event.target.checked,
      },
    }))
  }

  const handleChangeFilterValue: ChangeFilterValueFunctionType =
    fieldName => event => {
      setMonitorFilters(previousMonitorFilters => ({
        ...previousMonitorFilters,
        [fieldName]: {
          ...previousMonitorFilters[fieldName],
          value: event.target.value,
        },
      }))
    }

  const visibleMonitors = (): MonitorType[] => {
    let filteredMonitors = monitors

    if (monitorFilters.brand.active && monitorFilters.brand.value !== '')
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.brand === monitorFilters.brand.value
      )

    if (
      monitorFilters.minimumScreenSize.active &&
      monitorFilters.minimumScreenSize.value !== ''
    )
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.screenSize >= monitorFilters.minimumScreenSize.value
      )

    if (
      monitorFilters.maximumPrice.active &&
      monitorFilters.maximumPrice.value !== ''
    )
      filteredMonitors = filteredMonitors.filter(
        monitor => monitor.price <= monitorFilters.maximumPrice.value
      )

    return filteredMonitors
  }

  const handleSellMonitor: SellMonitorFunctionType = productNumber => {
    const soldMonitor = monitors.find(
      monitor => monitor.productNumber === productNumber && monitor.quantity > 0
    )

    if (!soldMonitor)
      return Promise.reject(
        `Monitor with product number '${productNumber}' not found or is out of stock`
      )

    setMonitors(previousMonitors =>
      previousMonitors
        .map(monitor =>
          monitor === soldMonitor
            ? { ...monitor, quantity: monitor.quantity - 1 }
            : monitor
        )
        .filter(monitor => monitor.quantity > 0)
    )

    return Promise.resolve(soldMonitor.quantity - 1)
  }

  return (
    <>
      <h1>{storeName}</h1>

      <NewMonitorForm onAddMonitor={handleAddMonitor} />
      <MonitorFilters
        monitorFilters={monitorFilters}
        onToggleFilter={handleToggleFilter}
        onChangeFilterValue={handleChangeFilterValue}
        monitors={monitors}
      />
      <MonitorInventory
        monitors={visibleMonitors()}
        onSellMonitor={handleSellMonitor}
      />
    </>
  )
}
