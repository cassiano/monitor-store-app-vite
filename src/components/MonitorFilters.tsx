import { ChangeFilterValueFunctionType } from './MonitorStore'
import {
  MonitorFiltersType,
  ToggleFilterFunctionType,
  MonitorType,
  BrandNameType,
  ScreenSizeType,
} from './MonitorStore'

type MonitorFilterProps = {
  monitorFilters: MonitorFiltersType
  monitors: MonitorType[]
  onToggleFilter: ToggleFilterFunctionType
  onChangeFilterValue: ChangeFilterValueFunctionType
}

export const MonitorFilters: React.FC<MonitorFilterProps> = ({
  monitorFilters,
  monitors,
  onToggleFilter,
  onChangeFilterValue,
}) => {
  const uniqueMonitorFields = (field: keyof MonitorType) => [
    ...new Set(monitors.map(monitor => monitor[field])),
  ]

  // Find unique values within all monitors.
  const uniqueMonitorBrands = uniqueMonitorFields('brand') as BrandNameType[]
  const uniqueMonitorScreenSizes = uniqueMonitorFields(
    'screenSize'
  ) as ScreenSizeType[]

  return (
    <fieldset>
      <legend>Filters</legend>

      <input
        type='checkbox'
        name='brandFilterChecked'
        checked={monitorFilters.brand.active}
        onChange={onToggleFilter('brand')}
      />
      <label htmlFor='brandFilterValue'>Brand: </label>
      <select name='brandFilterValue' onChange={onChangeFilterValue('brand')}>
        <option value=''>-- Select -- </option>
        {uniqueMonitorBrands.map(brand => (
          <option value={brand} key={brand}>
            {brand}
          </option>
        ))}
      </select>
      <br />

      <input
        type='checkbox'
        name='minimumSizeFilterChecked'
        checked={monitorFilters.minimumScreenSize.active}
        onChange={onToggleFilter('minimumScreenSize')}
      />
      <label htmlFor='minimumScreenSizeFilterValue'>
        Minimum Screen Size:{' '}
      </label>
      <select
        name='minimumScreenSizeFilterValue'
        onChange={onChangeFilterValue('minimumScreenSize')}
      >
        <option value=''>-- Select -- </option>
        {uniqueMonitorScreenSizes.map(screenSize => (
          <option value={screenSize} key={screenSize}>
            {screenSize}
          </option>
        ))}
      </select>
      <br />

      <input
        type='checkbox'
        name='maximumPriceFilterChecked'
        checked={monitorFilters.maximumPrice.active}
        onChange={onToggleFilter('maximumPrice')}
      />
      <label htmlFor='maximumPriceFilterValue'>Maximum Price: </label>
      <input
        type='number'
        name='maximumPriceFilterValue'
        onChange={onChangeFilterValue('maximumPrice')}
      />
      <br />
    </fieldset>
  )
}
