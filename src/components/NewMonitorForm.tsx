import { useState } from 'react'
import { AddMonitorFunctionType, MonitorType } from './MonitorStore'
import {
  BrandNameType,
  BRAND_NAMES,
  ScreenSizeType,
  SCREEN_SIZES,
} from './MonitorStore'

// Partial<MonitorType>
type MonitorDefaultFormDataType = {
  productNumber: string
  brand: BrandNameType | ''
  screenSize: ScreenSizeType
  price: number | ''
  quantity: number | ''
}

const MONITOR_DEFAULT_FORM_DATA: MonitorDefaultFormDataType = {
  productNumber: '',
  brand: '',
  screenSize: 21,
  price: '',
  quantity: '',
}

type NewMonitorFormProps = {
  onAddMonitor: AddMonitorFunctionType
}

export const NewMonitorForm: React.FC<NewMonitorFormProps> = ({
  onAddMonitor,
}) => {
  const [monitorFormData, setMonitorFormData] = useState(
    MONITOR_DEFAULT_FORM_DATA
  )

  const handleAddMonitorClick = (_event: React.MouseEvent<HTMLElement>) => {
    onAddMonitor(monitorFormData as MonitorType)
      .then(() =>
        // Monitor added sucessfully! Reset the form.
        setMonitorFormData(MONITOR_DEFAULT_FORM_DATA)
      )
      .catch(err => alert(err))
  }

  return (
    <fieldset>
      <legend>Add to inventory</legend>

      <label htmlFor='productNumber'>Product Number: </label>
      <input
        type='text'
        name='productNumber'
        value={monitorFormData.productNumber}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            productNumber: event.target.value,
          })
        }
      />
      <br />

      <label htmlFor='brand'>Brand: </label>
      <select
        name='brand'
        value={monitorFormData.brand}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            brand: event.target.value as MonitorDefaultFormDataType['brand'],
          })
        }
      >
        <option value=''>-- Select --</option>
        {BRAND_NAMES.map(brand => (
          <option key={brand}>{brand}</option>
        ))}
      </select>
      <br />

      <label htmlFor='screenSize'>Screen Size: </label>
      <select
        name='screenSize'
        value={monitorFormData.screenSize}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            screenSize: parseInt(
              event.target.value
            ) as MonitorDefaultFormDataType['screenSize'],
          })
        }
      >
        <option disabled={true} value=''>
          -- Select --
        </option>
        {SCREEN_SIZES.map(screenSize => (
          <option key={screenSize}>{screenSize}</option>
        ))}
      </select>
      <br />

      <label htmlFor='price'>Price: </label>
      <input
        type='number'
        name='price'
        value={monitorFormData.price}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            price: event.target.value !== '' ? +event.target.value : '',
          })
        }
      />
      <br />

      <label htmlFor='quantity'>Quantity: </label>
      <input
        type='number'
        name='quantity'
        value={monitorFormData.quantity}
        onChange={event =>
          setMonitorFormData({
            ...monitorFormData,
            quantity: event.target.value !== '' ? +event.target.value : '',
          })
        }
      />
      <br />

      <input
        type='button'
        value='Add Monitor'
        onClick={handleAddMonitorClick}
        disabled={
          !(
            monitorFormData.productNumber !== '' &&
            monitorFormData.brand !== '' &&
            monitorFormData.price !== '' &&
            monitorFormData.price > 0 &&
            monitorFormData.quantity !== '' &&
            monitorFormData.quantity > 0
          )
        }
      />
    </fieldset>
  )
}
